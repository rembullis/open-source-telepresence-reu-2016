#ifndef __CONTROLLER_H
#define __CONTROLLER_H

#include <stdio.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "360_controller_map.h"

/*
	This file provides a basic "controller" struct. Not much more, as it's fleshed out in 
	the "geekbot_controller" header.
*/

#define EVENT_BUTTON         0x01    // Button state update
#define EVENT_AXIS           0x02    // Axis state update
#define EVENT_INIT           0x80    // Non-input event, to be ignroed

#define MAX_BUTTON_COUNT        16
#define MAX_AXIS_COUNT          27   // 8 for 360, 27 for PS3

struct event {
	unsigned int time;	// Timestamp of when this event came in, in millis
	short value;   		// The event's associated value
	unsigned char type; // The event type, if button or axis
	unsigned char id;   // The event ID, tells which button or axis when used with a joystick
};

struct event event;

/*
	This is a struct to make handling controllers simpler. It provides a simple "object" to pass
	 around to functions.
*/
struct js_controller {
	char controller_location[128];           // Location to open the controller from, think /dev/input/js0
	int controller_id;                       // Wierd name, is actually the file descriptor with opened. Named as such to easily differentiate between multiple controllers
	unsigned char button[MAX_BUTTON_COUNT];  // Array of button values, as defined above
	short axis,[MAX_AXIS_COUNT];			 // Array of axis values, as defined above
	int output_id;							 // An OUTPUT TARGET file descriptor. Could be useful if using two controllers sending to two devices. 
};

/*
	This function "starts" the controller, opening its location and returning the fd
*/
int start_controller( struct js_controller *controller )
{
	controller->controller_id = open( controller->controller_location, O_RDONLY | O_NONBLOCK );
	if( controller->controller_id < 0 )
	{
		printf( "Unable to open controller at %s", controller->controller_location );
		return -1;
	}
	return controller->controller_id;
}

/*
	Checks the controller for any buffered inputs, updates the controller data accordingly
*/
int update_controller( struct js_controller *controller )
{
	int temp = controller->controller_id;
	if(controller->controller_id < 0) 
	{
		printf("Trying to update a non-existent controller!\n");
		return -1;
	}
	while( read(temp, &event, sizeof(event)) > 0 )
	{
		event.type &= ~EVENT_INIT; // ignore non-input events
		if ( event.type == EVENT_AXIS )
			controller->axis[event.id] = event.value; // update the corresponding axis/button with its new value
		if( event.type == EVENT_BUTTON )
			controller->button[event.id] = event.value;
		;
	}
	return 0;
}

/*
	Returns the respective button state
*/
unsigned char get_button_state( unsigned char button_id, struct js_controller *controller )
{
	return controller->button[button_id];
}

/*
	Returns the respective axis state
*/
short get_axis_state( unsigned char axis_id, struct js_controller *controller )
{
	return controller->axis[axis_id];
}

#endif