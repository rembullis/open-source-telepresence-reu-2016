#include <stdio.h>
//#include <fcntl.h>
#include "networking.h"
#include "easy_serial.h"

/* 
	Similar to the client run code, this takes two args: -p and -d. 
	The first is the (IP) port to listen on, the second is the serial port
	location (ala /dev/ttyUSB0).
*/

#define COMMAND_LENGTH 3
#define BAUDRATE 115200

// Struct for holding a full "command"
typedef struct command_struct{
	unsigned char flag; // Tells what to do with...
	short value;        // This information. 
} command;

/*
	This incredibly complicated (snort) function composes a two-byte int (a short, on most x86) from two bytes
*/
short make_int(unsigned char highbyte, unsigned char lowbyte)
{
    short val = 0;
    val |= highbyte;
    val <<= 8;
    val |= lowbyte;
	return val;
}

/*
	Reads n bytes from fd into dest. Crazy!
*/
int read_n_bytes(int fd, unsigned char *dest, unsigned int n)
{
    int read_bytes = 0;
    int num;
    while (read_bytes < n)
    {
        num = read(fd, dest+read_bytes, n-read_bytes);
        if (num < 1 )
        {
            printf("Unable to read from file, might be dead.\n");
            return 1;
        }
        read_bytes += num;
    }
    return 0;
}

/*
	Provides an easy way to grab one command from a file.
*/
int get_command(int fd, command *cmd)
{
	unsigned char temp[3] = {0};
	if(read_n_bytes(fd, temp, COMMAND_LENGTH))
		return 1;
	cmd->flag = temp[0];
	cmd->value = make_int(temp[2], temp[1]);
	return 0;
}

/*
	Sends a command, composed of a flag followed by a short value
*/
int send_command( unsigned char flag, short value, int send_port )
{
	unsigned char out_flag  = flag, i;
	short out_value = value;
	int n  =  write(send_port, &out_flag, 1);
	    n +=  write(send_port, &out_value, sizeof(out_value));
	return 0;
}

/*
	A *very* lazy function to handle command line args.
*/
int handle_args(int argc, char **argv, char *serial_port, int *port)
{
	int i;
	for(i = 1; i < argc; i++)
	{
		if(!strcmp(argv[i], "-d"))
			strcpy(serial_port, argv[i+1]);
		else if(!strcmp(argv[i], "-p"))
			*port = atoi(argv[i+1]);
		i++;
	}
}

int main(int argc, char **argv)
{
	if(argc < 3)
	{
		printf("You must provide an IP port on which to listen for connections (-p), and a TTY (-d) for comms!\n"); // You screwed up, give me more info.
		return 1;
	}

	char serial_port[64] = "/dev/ttyUSB0"; // Default ports, both IP and serial
	int port = 5000;

	handle_args(argc, argv, serial_port, &port); // Lazy AF

	int serialfd = serial_port_init(serial_port, BAUDRATE); // Attempt to start the serial connection
	if(serialfd < 1)
	{
		printf("Unable to open serial port, exiting...\n"); // Seppuku
		return 1;
	}

	printf("Serial port opened...\n");
	int serverfd = start_server(port); // Scary function call to "start the server", whatever that means. Check networking.h
	if(serverfd < 1)
	{
		printf("Error starting server: %i\n", serverfd);    // Suicide by code
		return 2;
	}

	printf("Server started fd = %i, waiting for connection on TCP port %i\n", serverfd, port);
	int streamfd = get_stream(serverfd); // If everything goes according to plan, the prog just sits here while waiting for a TCP bytestream from a client.
	if(streamfd < 1)
	{
		printf("Error getting stream from server: %i\n", streamfd); // Stuff's wrecked
		return 3;
	}

	command cmd; // Crazy, a variable!

	printf("Connected streamfd = %i, waiting for input from client\n", streamfd); // Things are working.
	while(1)
	{
		if(get_command(streamfd, &cmd) == 0)
            send_command(cmd.flag, cmd.value, serialfd); // Pump that command to the Arduino
		else 
		{	
			close_stream(streamfd); // If the stream can't be read, this loop is run. It closes the stream, and waits til a new stream is available.
			do
			{
				sleep(1);
				printf("Connection lost, waiting for new connection...\n");
				get_stream(serverfd);
			}
			while(streamfd < 1);
			printf("Successfully reconnected.\n"); // Hooray, you only screwed up a little bit. 
		}
		usleep(1 * 1000); // Help the scheduler do other computery things. If this isn't there one core gets pegged worse than a log cabin.
	}

	return 0;
}
