#ifndef __NETWORKING_H
#define __NETWORKING_H


#include <sys/socket.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h> 

/*
	SCARY. This header provides a simple way to connect and make TCP/IP connections.
*/

#define DEFAULT_PORT 5000

/*
	This ...thing sets up a TCP client connection to a server, given the IP as chars and a port.
*/
int start_client(char *ip, int port)
{
	int sockfd = 0;
	struct sockaddr_in serv_addr; // Bog standard struct for IP things

	if(ip == NULL) // Try giving me a *real* IP, thanks!
		return -1;

	sockfd = socket(AF_INET, SOCK_STREAM, 0); // Request a socket from the OS
	if(sockfd <= 0)
		return -2;

	serv_addr.sin_family = AF_INET; // Gimme TCP
	if(port <= 0)
		serv_addr.sin_port = htons(DEFAULT_PORT); // Set the port to supplied if port > 0
	else
		serv_addr.sin_port = htons(port);

	if(inet_pton(AF_INET, ip, &serv_addr.sin_addr) <= 0) // Get a correctly-formatted IP address into the struct. 
		return -3;

	if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)   // Try to make a connection to the IP and port we want, with the socket 
		return -4;																//  generate above as the medium.

	return sockfd;
}

/*
	This thing, similar to the above, starts the server on teh robot side. Beware.
*/
int start_server(int port)
{
	int serverfd = 0, connfd = 0;
	struct sockaddr_in serv_addr;

	serverfd = socket(AF_INET, SOCK_STREAM, 0); // The SOCKET fd

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY); // Connection setup crap
	if(port <= 0)
		serv_addr.sin_port = htons(DEFAULT_PORT);
	else
		serv_addr.sin_port = htons(port);

	bind(serverfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); // Yeah I wanna listen on this port and IP for a connection pl0x

    listen(serverfd, 10);                                            // Look mom, I'm telling the OS to listen to only 10 connections at once!

	return serverfd;
}

/*
	This kills the server (socket)
*/
int stop_server(int serverfd)
{
	close(serverfd);
	return 0;
}

/*
	This function actually acquires the connection with the remote client, returning the stream fd when a connection is made. 
*/
int get_stream(int serverfd)
{
	int streamfd = -1;

	do
	{
		streamfd = accept(serverfd, (struct sockaddr*)NULL, NULL); // SOMEONE ANYONE PLEASE RESPOND
	}
	while(streamfd < 1);

	return streamfd;
}

/*
	Kills the stream, no diff from above but in name for keeping calls straight
*/
int close_stream(int streamfd)
{
	close(streamfd);
	return 0;
}



#endif