#ifndef __GEEKBOT_CONTROLLER_H
#define __GEEKBOT_CONTROLLER_H

#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "easy_serial.h"
#include "controller.h"
#include "360_controller_map.h"
#include "networking.h"

/*
	This file fleshes out the baseline "controller" struct for use specifically with the geekbot.
	Obviously the attempted modular solution is a bit half-baked, but it works.
*/

#define BAUDRATE           115200 // The baudrate the robot will communicate on, obiously this and that should match.

#define FLAG_DRIVE_DRIVE   0x45   
#define FLAG_DRIVE_TURN    0x37
#define FLAG_DRIVE_LEFT    0x36
#define FLAG_DRIVE_RIGHT   0x35

#define FLAG_OUT_LIGHTS    0x30
#define FLAG_OUT_BUZZER    0x29

#define FLAG_CAM_PAN_HOR       0x28
#define FLAG_CAM_PAN_VERT      0x27
#define FLAG_CAM_ROLL          0x26

#define DEADZONE_HORIZONTAL_MIN  -6000
#define DEADZONE_HORIZONTAL_MAX   6000

#define DEADZONE_VERTICAL_MIN  -6000
#define DEADZONE_VERTICAL_MAX   6000

/*
	This bounds any input to within the range of a short
*/
short bound_short(int num)
{
	if( num > SHRT_MAX )
		return SHRT_MAX;
	else if(num < SHRT_MIN )
		return SHRT_MIN;
	else 
		return num;
}

/*
	Maps a value on an input range to an output range. Looks similar to the Arduino code, yeah?
*/
short map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/*
	Specifically maps to the range of a short, like above. Not *really* necessary but nice to have. 
*/
short short_map(long x)
{
  return (x - SHRT_MIN) * (2*SHRT_MAX - 2*SHRT_MIN) / (SHRT_MAX - SHRT_MIN) + 2*SHRT_MIN;
}

/*
	This is the function called when a signal is caught. SIGTERMS are *not* handled, be aware.
*/
void sig_handler(int signo)
{
  	if (signo == SIGINT)
  	{
   	 	printf("Received SIGINT, exiting.\n");
   		exit(1);
	}
	else
		printf("UNKNOWN SIGNAL\n");
	return;

}

/*
	Attempts to attach the sig_handler func as the signal handler callback.
*/
int sig_setup()
{
	if (signal(SIGINT, sig_handler) == SIG_ERR)
	{
  		printf("\ncan't catch SIGINT\n");
  		return 1;
	}
	return 0;
}

/*
	Sends a "command" composed of a flag and short value. Flags are defined above. 
*/
int send_command( unsigned char flag, short value, int send_port )
{
	unsigned char out_flag  = flag, i;
	short out_value = value;
	int n  =  write(send_port, &out_flag, 1);
	    n +=  write(send_port, &out_value, sizeof(out_value));
	return 0;
}

/*
	This function takes in an array of "old" button presses and compares the values against the "new" values stored in the input controller's array. 
	Upon detection of a new button press, the respective function inside the switch is called. There's NO doubt in my mind that there's a better way
	to do this, but this works for now.
*/
int execute_button_updates( unsigned char *old_button_values, 
							struct js_controller *controller )
{
	int button_id, pushed;
	for(button_id = 0; button_id < MAX_BUTTON_COUNT; button_id++)
		if(controller->button[button_id] != old_button_values[button_id])
		{
			old_button_values[button_id] = controller->button[button_id];
			pushed = controller->button[button_id];
			switch(button_id)
			{
				case BUTTON_A:
					break;
				case BUTTON_B:
					break;
				case BUTTON_X:
					break;
				case BUTTON_Y:
					break;
				case BUTTON_LEFT_BUMPER:
					if(pushed)
						send_command(FLAG_CAM_ROLL, SHRT_MIN, controller->output_id);
					else
						send_command(FLAG_CAM_ROLL, 0, controller->output_id);
					break;
				case BUTTON_RIGHT_BUMPER:
					if(pushed)
						send_command(FLAG_CAM_ROLL, SHRT_MAX, controller->output_id);
					else
						send_command(FLAG_CAM_ROLL, 0, controller->output_id);
					break;
				case BUTTON_LEFT_CLICK:
					if(pushed)
						send_command(FLAG_OUT_BUZZER, 1, controller->output_id);
					else
						send_command(FLAG_OUT_BUZZER, 0, controller->output_id);
					break;
				case BUTTON_RIGHT_CLICK:
					if(pushed)
						send_command(FLAG_OUT_LIGHTS, 1, controller->output_id);
					else
						send_command(FLAG_OUT_LIGHTS, 0, controller->output_id);
					break;
				case BUTTON_START:
					break;
				case BUTTON_BACK:
					break;
				case BUTTON_XBOX:
					break;
				default:
					printf("Button not supported.\n");
					break;
			}
		}
		return 0;
}

 /*
 	Check the above function, same idea.
 */
int execute_axis_updates( short  *old_axis_values, 
						  struct js_controller *controller )
{
	int axis_id;
	short value;
	for(axis_id = 0; axis_id < MAX_AXIS_COUNT; axis_id++)
		if(controller->axis[axis_id] != old_axis_values[axis_id])
		{
			old_axis_values[axis_id] = controller->axis[axis_id];
			value = controller->axis[axis_id];
			switch(axis_id)
			{
				case AXIS_LEFT_STICK_VERTICAL:
				case AXIS_LEFT_STICK_HORIZONTAL:;
				static  short left = 0, right = 0, new_right, new_left;
						int temp  = controller->axis[AXIS_LEFT_STICK_VERTICAL] + controller->axis[AXIS_LEFT_STICK_HORIZONTAL]; // Some ghetto code for arcade drive
						new_right = temp/2;
						temp = controller->axis[AXIS_LEFT_STICK_VERTICAL] - (controller->axis[AXIS_LEFT_STICK_HORIZONTAL]);
						new_left = temp/2;
					if( new_left != left )
					{
						send_command(FLAG_DRIVE_LEFT, new_left, controller->output_id);
						left = new_left;
					}
					if( new_right != right )
					{
						send_command(FLAG_DRIVE_RIGHT, new_right, controller->output_id);
						right = new_right;
					}
					break;
				case AXIS_RIGHT_STICK_VERTICAL:
					send_command(FLAG_CAM_PAN_VERT, value, controller->output_id);
					break;
				case AXIS_RIGHT_STICK_HORIZONTAL:
					send_command(FLAG_CAM_PAN_HOR, value, controller->output_id);
					break;
				case AXIS_LEFT_TRIGGER:
					value = map(value, SHRT_MIN, SHRT_MAX, 0, -SHRT_MAX);
					send_command(FLAG_CAM_ROLL, value, controller->output_id);
					break;
				case AXIS_RIGHT_TRIGGER:
					value = map(value, SHRT_MIN, SHRT_MAX, 0, SHRT_MAX);
					send_command(FLAG_CAM_ROLL, value, controller->output_id);
					break;
				default:
					printf("Axis not supported.\n");
					break;
			}
		}
		return 0;
}



#endif
