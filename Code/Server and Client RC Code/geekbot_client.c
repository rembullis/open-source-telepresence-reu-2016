#include <stdio.h>
#include "geekbot_controller.h"

/*
	This is the actual client run code. It takes three args: -j, -p, -a. These correspond to the 
	joystick location (ala /dev/input/js0), the outgoing port,a nd the outgoing address.
*/

// It's  a super lazy function to handle cmd line args! 
int handle_args(int argc, char **argv, char *joystick, char *ip, int *port)
{
	int i;
	for(i = 1; i < argc; i++)
	{
		if(!strcmp(argv[i], "-j"))
			strcpy(joystick, argv[i+1]);
		else if(!strcmp(argv[i], "-p"))
			*port = atoi(argv[i+1]);
		else if(!strcmp(argv[i], "-a"))
			strcpy(ip, argv[i+1]);
		i++;
	}
}

int main(int argc, char **argv)
{
	if(sig_setup()) // Assign the sighandler
		return 1; //GET OUT

	char server_address[64] = "127.0.0.1"; // Defaults for the server address, joystick loc, and port.
	char joystick[64] = "/dev/input/js0";
	int  port = -1;

	if(argc > 7)
	{
		printf("Too many arguments! Exiting."); // Mess with the best, die like the rest. 
		return 1;
	}

	handle_args(argc, argv, joystick, server_address, &port); // Laaaaaaaazy....
	
    printf("Trying to connect to %s:%i...\n", server_address, port);
	int robot_ip = start_client(server_address, port); // In this case robot_ip is a file descriptor, so bear with me. 
	if(robot_ip < 1)
    {
        printf("Unable to connect, returned fd = %i\n", robot_ip); // Shit's broke.
		return 2;
    }

	printf("Connected to server!\nrobot_ip=%i\n", robot_ip);

    short         old_axis_values[MAX_AXIS_COUNT] = {0};
    unsigned char old_button_values[MAX_BUTTON_COUNT] = {0}; // These are archives for past botton and axis values.
    struct js_controller xbox; // A controller struct, for a 360 controller though it's agnostic.

    strcpy(xbox.controller_location, joystick); // Update the controller.
    
    xbox.output_id = robot_ip; // Tell it where to spit commands. 

    if(start_controller(&xbox) < 0) // Try to open the controller desperately. 
    {
        printf("Controller no go, exiting...\n"); // /kills self
        return 1;
    }
    printf("Controller started, output_id = %i\n", xbox.output_id); // Things are going great fam. 

    while(1)
    {
        update_controller(&xbox);							// Check for new controller info
        execute_button_updates(old_button_values, &xbox);	// Use the new controller and axis values to do things
        execute_axis_updates(old_axis_values, &xbox);
        usleep(1000); 										// This keeps the computer working for longer than 30 seconds. 
    }
}
